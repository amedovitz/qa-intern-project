package program;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public abstract class BaseSpecClass {

    public WebDriver driver;
    public Properties locators;
    public WebDriverWait waiter;

//    public void choosingWebDriver(String driver){
//        if(driver.equalsIgnoreCase("Chorme")){
//            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
//                    //"webdriver.chrome.driver",
//                    "C:\\Users\\Nedin tata\\ChromeDriver\\chromedriver.exe");
//            this.driver = new ChromeDriver();
//        }else if(driver.equalsIgnoreCase("Firefox")){
//            System.setProperty("webdriver.gecko.driver", "C:\\Users\\Nedin tata\\FireFoxDriver.geckodriver.exe");
//            this.driver = new FirefoxDriver();
//        }else if(driver.equalsIgnoreCase("Safari")){
//            this.driver = new SafariDriver();
//        }
//    }
    @BeforeMethod

   // @BeforeClass
    public void setup() throws IOException {
        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
                //"webdriver.chrome.driver",
                "C:\\Users\\Nedin tata\\ChromeDriver\\chromedriver.exe");

        this.driver = new ChromeDriver();
        this.locators = new Properties();
        this.waiter = new WebDriverWait(driver, 30);
        this.locators.load(new FileInputStream("config/page.properties"));
        this.driver.manage().window().maximize();
    }
 @AfterClass
    public void closeDriver(){
        driver.close();
    }

}
