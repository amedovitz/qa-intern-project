package program;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Locators extends BaseSpecClass {

    @Test
    public void setUpForLocators() throws IOException {
        locators.load(new FileInputStream("config/page.properties"));
        this.driver.navigate().to(this.locators.getProperty("url"));
//        this.driver.navigate().to("https://www.guru99.com/");
    }

    public void findElements(){
        WebElement searchBar = this.driver.findElement(By.id("gsc-i-id2"));
        WebElement searchBtn = this.driver.findElement(By.xpath
                ("//*[@id=\"___gcse_1\"]/div/div/form/table/tbody/tr/td[2]/button"));
        WebElement sqlDatabase = this.driver.findElement(By.xpath("//*[@id=\"java_technologies\"]/li[1]/a"));
        WebElement linuxCourse = this.driver.findElement(By.linkText("/unix-linux-tutorial.html"));
    }
}
