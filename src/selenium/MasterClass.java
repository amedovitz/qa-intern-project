package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


import java.io.IOException;


public abstract class MasterClass {

    public WebDriver driver;

    public WebDriverWait waiter;



    @BeforeClass
    public void setup() throws IOException {
        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
                //"webdriver.chrome.driver",
                "C:\\Users\\slash\\Downloads\\chromedriver_win32\\chromedriver.exe");

        this.driver = new ChromeDriver();

        this.waiter = new WebDriverWait(driver, 100);

        this.driver.manage().window().maximize();

    }
    @AfterClass
        public void closeDriver(){
        driver.close();
    }

}
