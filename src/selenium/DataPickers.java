package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class DataPickers extends MasterClass{

    private final By dropDownMenu = By.xpath("//*[@id=\"navbar-brand-centered\"]/ul[1]/li[2]/a");
    private final By  bootStrapDate = By.xpath("//*[@id=\"navbar-brand-centered\"]/ul[1]/li[2]/ul/li[1]/a");
    private final By  calendar = By.xpath("//*[@id=\"sandbox-container1\"]/div/span");
    private final By  day = By.xpath("/html/body/div[3]/div[1]/table/tbody/tr[1]/td[4]");
    private final By  jQueryDate = By.xpath("//*[@id=\"navbar-brand-centered\"]/ul[1]/li[2]/ul/li[2]/a");
    private final By  dateRangePicker = By.xpath("/html/body/div[2]/div/div[2]/div/div[1]");
    private final By  dateFrom = By.id("from");
    //private final By  testing = By.("from");
    private final By  dateTo = By.id("to");
    private final By popUpBoxX = By.xpath("//*[@id=\"at-cv-lightbox-close\"]");

    @BeforeMethod
    public void setupForTests(){
        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(By.xpath("//*[@id=\"treemenu\"]/li/ul/li[1]/a")).click();
        WebElement popUp;
        waiter.withTimeout(Duration.ofSeconds(1));
        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
        Assert.assertEquals(actual,expected);
    }

    @Test
    public void clickBootStrapDataPicker(){

        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(dropDownMenu).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        driver.findElement(bootStrapDate).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        //driver.findElement(By.xpath("//*[@id=\"navbar-brand-centered\"]/ul[1]/li[2]/ul/li[1]/a")).click();
        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html";
        Assert.assertEquals(actual,expected);

    }

    @Test
    public void clickJQDatePicker(){

        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        //driver.findElement(By.xpath("//*[@id=\"treemenu\"]/li/ul/li[1]/a")).click();
        driver.findElement(dropDownMenu).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        driver.findElement(jQueryDate).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/jquery-date-picker-demo.html";
        Assert.assertEquals(actual,expected);
    }

    @Test
    public void selectJQdate(){

        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(dropDownMenu).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        driver.findElement(jQueryDate).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        driver.findElement(dateFrom).click();
        WebElement fromDate = driver.findElement(dateFrom);
        WebElement toDate = driver.findElement(dateTo);
        fromDate.sendKeys("04/01/2021");
        toDate.sendKeys("07/01/2021");
        driver.findElement(dateRangePicker).click();
    }

    @Test
    public void selectBootStrapDate(){

        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(dropDownMenu).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        driver.findElement(bootStrapDate).click();
        waiter.withTimeout(Duration.ofSeconds(1));
        //driver.findElement(By.xpath("//*[@id=\"navbar-brand-centered\"]/ul[1]/li[2]/ul/li[1]/a")).click();
        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html";
        Assert.assertEquals(actual,expected);
    }
}
