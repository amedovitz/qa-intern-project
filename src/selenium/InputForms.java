package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class InputForms extends MasterClass{
    @BeforeMethod
    public void setupForTests(){
        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(By.xpath("//*[@id=\"treemenu\"]/li/ul/li[1]/a")).click();
        WebElement popUp;
        popUp = waiter.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"at-cv-lightbox-close\"]")));
        popUp.click();
    }
    @Test
    public void validatingCheckBoxDemoPage (){
        driver.findElement(By.xpath("//*[@id=\"treemenu\"]/li/ul/li[1]/ul/li[2]/a")).click();
        boolean isCheckbox = this.driver.getCurrentUrl().contains("checkbox");
        Assert.assertTrue(isCheckbox, "Wrong page");
    }
    @Test
    public void selectingDayFromSelectMenu(){
        driver.findElement(By.xpath("//*[@id=\"treemenu\"]/li/ul/li[1]/ul/li[4]/a")).click();
        WebElement days = driver.findElement(By.xpath("//*[@id=\"select-demo\"]"));
        days.click();
        Select dayOfWeek = new Select(days);
        dayOfWeek.selectByValue("Friday");
        WebElement selectedDay = driver.findElement(By.xpath("//*[@id=\"easycont\"]/div/div[2]/div[1]/div[2]/p[2]"));
        Assert.assertTrue(selectedDay.isDisplayed(), "Sorry, try again");
    }




}
