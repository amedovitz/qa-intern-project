package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Table extends MasterClass{

    @Test (priority = 0)
    public void setUpForLocators(){
        driver.navigate().to("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
    }

    public void popUpMessage(){

        driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div/div[3]/div[2]/div[2]/div/a[2]")).click();
    }

    @Test
    public void clickTablePagination(){

        try{
            popUpMessage();
        } catch (Exception e){
        driver.findElement(By.xpath(Properties.TABLE)).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/nav/div/div[2]/ul[1]/li[3]/ul/li[1]/a")).click();
        }

        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/table-pagination-demo.html";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void clickTableDataSearch(){

        try {
            popUpMessage();
        } catch (Exception e) {
        driver.findElement(By.xpath(Properties.TABLE)).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/nav/div/div[2]/ul[1]/li[3]/ul/li[2]/a")).click();
        }

        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/table-search-filter-demo.html";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void clickTableFilter(){

       try {
           popUpMessage();
       } catch (Exception e) {
        driver.findElement(By.xpath(Properties.TABLE)).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/nav/div/div[2]/ul[1]/li[3]/ul/li[3]/a")).click();
       }

        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/table-records-filter-demo.html";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void clickSortAndSearch(){

       try{
           popUpMessage();
       } catch (Exception e) {
        driver.findElement(By.xpath(Properties.TABLE)).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/nav/div/div[2]/ul[1]/li[3]/ul/li[4]/a")).click();
       }

        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/table-sort-search-demo.html";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void clickDataDownload(){

        try {
            popUpMessage();
        } catch (Exception e) {
        driver.findElement(By.xpath(Properties.TABLE)).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/nav/div/div[2]/ul[1]/li[3]/ul/li[5]/a")).click();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String actual = driver.getCurrentUrl();
        String expected = "https://www.seleniumeasy.com/test/table-data-download-demo.html";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void selectEntries(){

        try {
            popUpMessage();
        } catch (Exception e) {
            driver.get("https://www.seleniumeasy.com/test/table-sort-search-demo.html");
            Select se = new Select(driver.findElement(By.name("example_length")));
            se.selectByVisibleText("100");
            WebElement selected =  driver.findElement(By.name("example_length"));

            try {
                Thread.sleep(3000);
            } catch (InterruptedException b) {
                e.printStackTrace();
            }
            Assert.assertTrue(selected.isDisplayed(), "Wrong choice");
        }
    }

}
